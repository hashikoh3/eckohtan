class Potepan::CategoriesController < ApplicationController
  helper_method :count_number_of_products_by_option
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(:root)
    @colors = Spree::OptionValue.values_all("Color")
    @sizes = Spree::OptionValue.values_all("Size")

    if params[:option].present?
      @products =  @taxon.all_products.includes(master: [:default_price, :images]).filter_by_option(params[:option])
    elsif params[:order].present?
      @products =  @taxon.all_products.includes(master: [:default_price, :images]).filter_by_order(params[:order])
    else
      @products =  @taxon.all_products.includes(master: [:default_price, :images]).order(available_on: :desc)
    end
  end

  def count_number_of_products_by_option(option_value)
    @taxon.all_products.includes(variants: :option_values).where(spree_option_values: { name: option_value.name }).count
  end
end
