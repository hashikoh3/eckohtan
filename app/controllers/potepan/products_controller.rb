class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @product_properties = @product.product_properties.includes(:property)
    @related_products = @product.related_products.limit(MAX_DISPLAY_RELATED_PRODUCTS)
  end

  def search
    @search = @q.result.includes(master: [:default_price, :images]).page(params[:page])
  end
end
