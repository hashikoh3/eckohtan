Spree::OptionValue.class_eval do
  scope :values_all, -> (value) { includes(:option_type).where(spree_option_types: { presentation: value }) }
end
