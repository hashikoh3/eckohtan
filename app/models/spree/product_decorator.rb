Spree::Product.class_eval do
  def related_products
    Spree::Product.in_taxons(taxons).includes(master: [:default_price, :images]).where.not(id: id).distinct
  end

  def self.ransackable_attributes(auth_object = nil)
    %w(name description)
  end

  def ransackable_associations(auth_object = nil)
    %w(stores variants_including_master master variants)
  end

  scope :filter_by_option, -> (option) { joins(variants: :option_values).where(spree_option_values: { name: option }).distinct }
  scope :filter_by_order, -> (order) {
    case order
    when "arrivals_desc"
      order(available_on: :desc)
    when "arrivals_asc"
      order(available_on: :asc)
    when "low_price"
      order('spree_prices.amount asc')
    when "high_price"
      order('spree_prices.amount desc')
    end
  }
end
