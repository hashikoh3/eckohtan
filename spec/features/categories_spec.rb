require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:taxonomy) { create(:taxonomy) }
  let!(:option_type) { create(:option_type, presentation: "Color") }
  let!(:option_value) { create(:option_value, option_type: option_type) }
  let!(:variant) { create(:variant, option_values: [option_value]) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:product2) { create(:product, variants: [variant], taxons: [taxon]) }

  scenario "categories/show has taxon product" do
    visit potepan_category_path(taxon.id)
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price

    click_on product.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end

  scenario "category has option_value" do
    visit potepan_category_path(taxon.id)
    expect(page).to have_link option_value.name
    click_on option_value.name
    expect(page).to have_current_path("/potepan/categories/#{taxon.id}?color=#{option_value.name}")
    expect(page).to have_content product2.name
  end
end
