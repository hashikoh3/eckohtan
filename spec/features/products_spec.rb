require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:taxon1) { create(:taxon) }
  let!(:taxon2) { create(:taxon) }
  let!(:option_type) { create(:option_type, presentation: "Color") }
  let!(:product1) { create(:product, taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "RUBY ON RAILS", taxons: [taxon1]) }

  scenario "product template with related_products" do
    visit potepan_product_path product1.id
    within(".media") do
      expect(page).to have_content product1.name
      expect(page).to have_content product1.display_price
    end
    within(".productBox") do
      expect(page).to have_content product2.name
      expect(page).to have_content product2.display_price
    end
  end

  scenario "click_on product2 and move to product2 page" do
    visit potepan_product_path product1.id
    within(".productBox") do
      click_on product2.name
      expect(current_path).to eq potepan_product_path(product2.id)
    end
  end

  scenario "click_on 一覧ページへ戻る" do
    visit potepan_product_path product1.id
    within(".media-body") do
      click_on "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(product1.taxons.first.id)
    end
  end

  scenario "search product" do
    visit potepan_root_path
    fill_in "product_name", with: "Rails"
    click_on "検索"
    expect(current_path).to eq search_potepan_products_path
    expect(page).to have_content product2.name
  end
end
