require 'rails_helper'

RSpec.describe Spree::OptionType, type: :model do
  describe "colors" do
    let!(:option_value) { create(:option_value) }
    let!(:option_type) { create(:option_type, option_values: [option_value], presentation: "Color") }

    it "presentation color" do
      expect(Spree::OptionValue.values_all("Color")).to contain_exactly(option_value)
    end
  end

  describe "sizes" do
    let!(:option_value) { create(:option_value) }
    let!(:option_type) { create(:option_type, option_values: [option_value], presentation: "Size") }

    it "presentation size" do
      expect(Spree::OptionValue.values_all("Size")).to contain_exactly(option_value)
    end
  end
end
