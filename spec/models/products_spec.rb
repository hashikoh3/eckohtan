require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let!(:taxon) { create(:taxon) }
  let!(:taxon1) { create(:taxon) }
  let!(:taxon2) { create(:taxon) }
  let!(:taxon3) { create(:taxon) }
  let!(:taxon4) { create(:taxon) }
  let!(:option_type) { create(:option_type) }
  let!(:option_value) { create(:option_value) }
  let!(:variant) { create(:variant, option_values: [option_value], product: product5) }
  let!(:product) { create(:product, taxons: [taxon1, taxon2, taxon3]) }
  let!(:product1) { create(:product, taxons: [taxon1]) }
  let!(:product2) { create(:product, taxons: [taxon2]) }
  let!(:product3) { create(:product, taxons: [taxon3]) }
  let!(:product4) { create(:product, taxons: [taxon4]) }
  let!(:product5) { create(:product) }

  describe "related_products" do
    it "check related_products method" do
      expect(product.related_products).to contain_exactly(product1, product2, product3)
      expect(product.related_products).not_to contain_exactly(product4)
    end
  end

  describe "filter by option" do
    it "product filter by option" do
      expect(Spree::Product.includes(master: [:default_price, :images]).filter_by_option(option_value.name)).to contain_exactly(product5)
    end
  end

  describe "products filter by order" do
    context "sort to list" do
      let!(:product) { create(:product, id: 1, taxons: [taxon], available_on: Time.current.ago(1.hour)) }
      let!(:product_2) { create(:product, id: 2, taxons: [taxon], available_on: Time.current.ago(2.hour)) }

      it "order by desc" do
        expect(taxon.all_products.includes(master: [:default_price, :images]).filter_by_order("arrivals_desc").map(&:id)).to eq [1, 2]
      end

      it "order by asc" do
        expect(taxon.all_products.includes(master: [:default_price, :images]).filter_by_order("arrivals_asc").map(&:id)).to eq [2, 1]
      end
    end
  end
end
