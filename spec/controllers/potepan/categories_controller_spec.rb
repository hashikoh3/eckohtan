require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxon1) { create(:taxon, taxonomy: taxonomy) }
    let!(:taxon2) { create(:taxon, taxonomy: taxonomy) }
    let!(:taxonomy) { create(:taxonomy) }
    let!(:product1) { create(:product, taxons: [taxon1]) }
    let!(:product2) { create(:product, taxons: [taxon2]) }

    before do
      get :show, params: { id: taxon1.id }
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "assign instance taxon" do
      expect(assigns(:taxon)).to eq taxon1
    end

    it "render show template" do
      expect(response).to render_template :show
    end

    it "assign instance taxonomies" do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it "assign instance products" do
      expect(assigns(:products)).to match_array product1
    end
  end
end
